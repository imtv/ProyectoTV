// Cargar modulos y crear nueva aplicacion
var express = require("express");
var app = express();
var cors = require("cors");
var bodyParser = require("body-parser");
var bodyParser = require("body-parser");


app.use(bodyParser.json()); // soporte para bodies codificados en jsonsupport
app.use(bodyParser.urlencoded({ extended: true })); // soporte para bodies codificados
app.use(cors());

const mysql = require("mysql");

// mysql://b810a0c3a5d6e9:ca0664f4@us-cdbr-iron-east-02.cleardb.net/heroku_671d824d692e205?reconnect=true
var mysqlConnection;

function crearConexionBD() {
  mysqlConnection = mysql.createConnection({
    host: "us-cdbr-iron-east-02.cleardb.net",
    user: "b810a0c3a5d6e9",
    password: "ca0664f4",
    database: "heroku_671d824d692e205"
  });

  mysqlConnection.connect(function (err) {
    if (err) {
      console.log("error when connecting to db:", err);
    } else {
      console.log("Se conectó a la Base de Datos");
    }
  });
}

app.listen(8090, () => {
  console.log("Hola :8090");
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());




//Ejemplo: GET http://localhost:8080
app.get("/estudiantes", function (req, res, next) {
  crearConexionBD();
  mysqlConnection.query("SELECT * FROM estudiante", (err, rows, fields) => {
    if (err) {
      console.log("=>" + err);
    } else {
      res.json(rows);
    }
    mysqlConnection.end();
  });
});

app.get("/comentarios", function (req, res, next) {
  crearConexionBD();
  mysqlConnection.query("SELECT * FROM comentario", (err, rows, fields) => {
    if (err) {
      console.log("=>" + err);
    } else {
      res.json(rows);
    }
    mysqlConnection.end();
  });
});

app.get("/comentarios/estudiantes/:fkVideo", function (req, res, next) {
  fkVideo = req.params.fkVideo;

  crearConexionBD();
  mysqlConnection.query(
    "SELECT * FROM comentario, estudiante WHERE comentario.fkEstudiante=estudiante.idEstudiante " +
    "AND comentario.fkVideo = " + fkVideo + " order by -comentario.tiempo",
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.json(rows);
      }
      mysqlConnection.end();
    }
  );
});

// app.get("/comentarios/estudiantes/:idVideo", function(req, res, next) {
//   idVideo = req.params.idVideo;

//   crearConexionBD();
//   mysqlConnection.query(
//     "SELECT * FROM comentario, estudiante WHERE comentario.fkEstudiante=estudiante.idEstudiante AND comentario.fkVideo=" +
//       idVideo,
//     (err, rows, fields) => {
//       if (err) {
//         console.log("=>" + err);
//       } else {
//         res.json(rows);
//       }
//       mysqlConnection.end();
//     }
//   );
// });

app.get("/comentarios/estudiantes/:fkVideo/:cantDatos", function (req, res, next) {
  cantDatos = req.params.cantDatos;
  fkVideo = req.params.fkVideo;
  query = "";
  if (fkVideo == 0) {
    query = "SELECT * FROM comentario, estudiante WHERE comentario.fkEstudiante=estudiante.idEstudiante order by -comentario.tiempo limit " + cantDatos;
  } else {
    query = "SELECT * FROM comentario, estudiante WHERE comentario.fkEstudiante=estudiante.idEstudiante " +
      "AND comentario.fkVideo = " + fkVideo + " order by -comentario.tiempo limit " + cantDatos;
  }
  crearConexionBD();
  mysqlConnection.query(
    query,
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.json(rows);
      }
      mysqlConnection.end();
    }
  );
});

//Ejemplo: POST http://localhost:8080/insert/estudiante
app.post("/insert/estudiante", function (req, res) {
  crearConexionBD();
  const estudiante = req.body;
  const query =
    "INSERT INTO estudiante " +
    "(`nombreEstudiante`, `apellidoEstudiante`, `codigoEstudiante`) " +
    "VALUES (?, ?, ?)";
  mysqlConnection.query(
    query,
    [estudiante.nombre, estudiante.apellido, estudiante.codigo],
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.send(rows);
      }
      mysqlConnection.end();
    }
  );
});

//Ejemplo: POST http://localhost:8080/
app.post("/insert/comentario", function (req, res) {
  crearConexionBD();
  const comentario = req.body;
  const query =
    "INSERT INTO comentario " +
    "(`comentario`, `fkEstudiante`, `fkVideo`, `tiempo`) " +
    "VALUES (?, ?, ?, ?)";
  mysqlConnection.query(
    query,
    [comentario.comentario, comentario.fkEstudiante, comentario.fkVideo, comentario.tiempo],
    (err, rows, fields) => {
      if (err) {
        console.log("=>" + err);
      } else {
        res.send(rows);
      }
      mysqlConnection.end();
    }
  );
});

//Ejemplo: PUT http://localhost:8080/items
app.put("/items", function (req, res) {
  var itemId = req.body.id;
  var data = req.body.data;
  res.send("Update " + itemId + " with " + data);
});

//Ejemplo: DELETE http://localhost:8080/items
app.delete("/items/:id", function (req, res) {
  var itemId = req.params.id;
  res.send("Delete " + itemId);
});

var server = app.listen(8080, function () {
  console.log("Server is running..");
});
