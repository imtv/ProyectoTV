const mysql = require('mysql');

// mysql://b810a0c3a5d6e9:ca0664f4@us-cdbr-iron-east-02.cleardb.net/heroku_671d824d692e205?reconnect=true
var mysqlConnection;

function handleDisconnect() {
    mysqlConnection = mysql.createConnection({
        host: 'us-cdbr-iron-east-02.cleardb.net',
        user: 'b810a0c3a5d6e9',
        password: 'ca0664f4',
        database: 'heroku_671d824d692e205'
    });

    mysqlConnection.connect(function (err) {
        if (err) {
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000);
        } else {
            console.log('Se conectó a la Base de Datos');
        }
    });

    // If you're also serving http, display a 503 error.
    mysqlConnection.on('error', function (err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a
        } else {                                      // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
    });
}

handleDisconnect();

module.exports = mysqlConnection;