//'https://dog.ceo/api/breeds/image/random'
const AXIOS = require('axios');

class UtilAPI {

    constructor({ url }) {
        this.url = url;
        this.endpoints = {};
    }

    createEntity(entity) {
        const name = entity.name;
        const request = entity.request;
        this.endpoints[name] = this.consultarAPIImagenes(request);
    }

    createEntities(arrayOfEntity) {
        arrayOfEntity.forEach(this.createEntity.bind(this));
    }

    consultarAPIImagenes(request) {
        var endpoints = {};

        const resourceURL = `${this.url}${request}`;

        endpoints.getAll = () => AXIOS.get(resourceURL);

        return endpoints
    }


}

export default UtilAPI;