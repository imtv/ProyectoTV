import * as firebase from "firebase";
import React, { Component } from "react";
import Loadable from "react-loadable";
import { HashRouter, Route, Switch } from "react-router-dom";
import "./App.css";

var config = {
  apiKey: "AIzaSyCz-Sf6gnfUmHw3tG8zVyr6gA-95g6_Sh4",
  authDomain: "proyectotv-c83e4.firebaseapp.com",
  databaseURL: "https://proyectotv-c83e4.firebaseio.com",
  projectId: "proyectotv-c83e4",
  storageBucket: "proyectotv-c83e4.appspot.com",
  messagingSenderId: "97019704245"
};
firebase.initializeApp(config);

const loading = () => (
  <div className="rel-cargando">
    <div id="loading" />
  </div>
);

const Login = Loadable({
  loader: () => import("./vista/Login/Login"),
  loading
});

const Home = Loadable({
  loader: () => import("./vista/Home/Home"),
  loading
});

const TeacherHome = Loadable({
  loader: () => import("./vista/Teacher/Home/Home"),
  loading
});

const TeacherClass = Loadable({
  loader: () => import("./vista/Teacher/Class/Class"),
  loading
});

const TeacherTopic = Loadable({
  loader: () => import("./vista/Teacher/Topic/Topic"),
  loading
});

const TeacherResultResume = Loadable({
  loader: () => import("./vista/Teacher/ResultResume/ResultResume"),
  loading
});

const TeacherResultIndividual = Loadable({
  loader: () => import("./vista/Teacher/ResultIndividual/ResultIndividual"),
  loading
});

const TeacherComments = Loadable({
  loader: () => import("./vista/Teacher/Comments/Comments"),
  loading
});

const InteractiveSound = Loadable({
  loader: () => import("./vista/InteractiveSound/InteractiveSound"),
  loading
});

const InteractiveGesture = Loadable({
  loader: () => import("./vista/InteractiveGesture/InteractiveGesture"),
  loading
});

const Prueba = Loadable({
  loader: () => import("./vista/Components/SlideComments/SlideComments"),
  loading
});

const VideoContent = Loadable({
  loader: () => import("./vista/VideoContent/VideoContent"),
  loading
});

const VideoHardWare = Loadable({
  loader: () => import("./vista/Interaccion1/Home"),
  loading
});

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route
            exact
            path="/Home"
            name="Home - YOURLEARN"
            component={Home}
          />

          <Route
            exact
            path="/Prueba"
            name="Home - YOURLEARN"
            component={Prueba}
          />

          <Route
            exact
            path="/InteractiveSound"
            name="Home - YOURLEARN"
            component={InteractiveSound}
          />
          <Route
            exact
            path="/InteractiveGesture"
            name="Interaccion con gestura - YOURLEARN"
            component={InteractiveGesture}
          />
          <Route exact path="/" name="Login - IMUAO" component={Login} />
          <Route
            exact
            path="/Teacher/Home"
            name="Home"
            component={TeacherHome}
          />
          <Route
            exact
            path="/Teacher/Class"
            name="Class"
            component={TeacherClass}
          />
          <Route
            exact
            path="/Teacher/Topic"
            name="Topic"
            component={TeacherTopic}
          />
          <Route
            exact
            path="/Teacher/ResultResume"
            name="Resume"
            component={TeacherResultResume}
          />
          <Route
            exact
            path="/Teacher/ResultIndividual"
            name="Individual"
            component={TeacherResultIndividual}
          />
          <Route
            exact
            path="/Teacher/Comments"
            name="Comments"
            component={TeacherComments}
          />
          <Route
            exact
            path="/VideoContent"
            name="Comments"
            component={VideoContent}
          />
          <Route
            exact
            path="/VideoHardWare"
            name="VideoHardWare"
            component={VideoHardWare}
          />
        </Switch>
      </HashRouter>
    );
  }
}

export default App;
