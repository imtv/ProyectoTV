import React, { Component } from 'react';
import './NavResult.css';

class NavResult extends Component {

    constructor() {
        super();
        this.state = {
            chartData: {}
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark">
                <div className="btn">22 respuestas</div>
                <div className="collapse justify-content-center navbar-collapse" id="navbarNav">
                    <div className="btn-group" role="group" aria-label="Botones">
                        <button type="button" className={this.props.resumen} onClick={this.props.resumenBt}>Resumen</button>
                        <button type="button" className={this.props.individual} onClick={this.props.individualBt}>Individual</button>
                    </div>
                </div>
            </nav>
        );
    }
}

export default NavResult;