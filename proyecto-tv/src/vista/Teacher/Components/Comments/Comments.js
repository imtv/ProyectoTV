import React, { Component } from 'react';
import './Comments.css';
import CommentTeacher from "../Comments/Comment-Teacher/Comment-Teacher"

class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const comentarios = this.props.listaComentarios;
    const listItems = comentarios.map((comentario) => {
        const tiempo = new Date(parseInt(comentario.tiempo) * 1000);
        const mes = tiempo.getMonth() + 1;
        const dia = tiempo.getDate();
        const anio = tiempo.getFullYear();
        const fecha = mes + "/" + dia + "/" + anio

        console.log(comentario);
        

        return (
            <CommentTeacher
                imagen={comentario.foto}
                nombre={comentario.nombreEstudiante}
                comentario={comentario.comentario}
                tiempo={fecha}
                key={comentario.idComentario}
            />
        )
    });

    return (
        <ul>{listItems}</ul>
    );
  }
}

export default Comments;