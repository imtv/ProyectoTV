import React, { Component } from "react";
import "./Topic.css";
import Header3 from "../Components/Header3/Header3";
import Chart from "../Components/Chart/Chart";
import Comment from "../Components/Comments/Comment/Comment";
import axios from "axios";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      chartData: {},
      comentEstudiantes: []
    };
  }

  componentWillMount() {
    this.getChartData();
    axios
      .get("http://192.168.1.22:8090/comentarios/estudiantes/0/4")
      .then(response =>
        this.setState({
          comentEstudiantes: response.data
        })
      )
      .catch(e => console.log(e));
  }

  getChartData() {
    // Ajax calls here
    this.setState({
      chartData: {
        labels: ["A", "B", "C", "D", "E", "F"],
        datasets: [
          {
            label: "Estudiantes",
            data: [10, 2, 15, 1, 0, 6],
            backgroundColor: [
              "rgba(255, 99, 132, 0.6)",
              "rgba(54, 162, 235, 0.6)",
              "rgba(255, 206, 86, 0.6)",
              "rgba(75, 192, 192, 0.6)",
              "rgba(153, 102, 255, 0.6)",
              "rgba(255, 159, 64, 0.6)",
              "rgba(255, 99, 132, 0.6)"
            ]
          }
        ]
      }
    });
  }

  handleSubmitResume = event => {
    this.props.history.push("/Teacher/Topic");
  };

  handleSubmitResults = event => {
    this.props.history.push("/Teacher/ResultResume");
  };

  handleSubmitComments = event => {
    this.props.history.push("/Teacher/Comments");
  };

  render() {
    return (
      <div>
        <Header3
          resumen={"nav-item active"}
          respuestas={"nav-item"}
          comentarios={"nav-item"}
          handleSubmitResume={this.handleSubmitResume}
          handleSubmitResults={this.handleSubmitResults}
          handleSubmitComments={this.handleSubmitComments}
        />
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="row">
                <div className="shadow-sm p-3 mt-3 bg-white rounded w-100">
                  <div className="px-3 py-3">
                    <h6 className="font-weight-bold text-primary">
                      En lo que mas se equivocaron
                    </h6>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <Chart
                        chartData={this.state.chartData}
                        location="Que es un sistema multimedia"
                        legendPosition="bottom"
                      />
                    </div>
                    <div className="col-6">
                      <Chart
                        chartData={this.state.chartData}
                        location="Que es un sistema multimedia"
                        legendPosition="bottom"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="shadow-sm p-3 mt-3 bg-white rounded w-100">
                  <div className="px-3 py-3">
                    <h6 className="font-weight-bold text-primary">
                      Ultimos comentarios
                    </h6>
                  </div>
                  <div className="row">
                    {this.state.comentEstudiantes.map(date => {
                      const tiempo = new Date(parseInt(date.tiempo) * 1000);
                      const mes = tiempo.getMonth() + 1;
                      const dia = tiempo.getDate();
                      const anio = tiempo.getFullYear();
                      const fecha = mes + "/" + dia + "/" + anio;
                      return (
                        <Comment
                          key={date.idComentario}
                          imagen={date.foto}
                          nombre={
                            date.nombreEstudiante +
                            " " +
                            date.apellidoEstudiante
                          }
                          comentario={date.comentario}
                          tiempo={fecha}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
