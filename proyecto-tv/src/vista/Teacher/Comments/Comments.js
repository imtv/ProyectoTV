import React, { Component } from "react";
import "./Comments.css";
import Header3 from "../Components/Header3/Header3"
import Comments from "../Components/Comments/Comments"
import { Player, ControlBar } from "video-react";
import "video-react/dist/video-react.css";
import UtilBaseDatos from "../../../control/UtilBaseDatos";
import axios from "axios";

const sources = {
    sintelTrailer: "./assets/video/Que_es_Multimedia.mp4",
    bunnyTrailer: "./assets/video/Curso-React-Introduccion.mp4"
};

export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            source: sources["sintelTrailer"],
            listaComentarios: [],
            listaUsuarios: []
        };
    }

    componentDidMount() {
        // this.timerID = setInterval(
        //     () => this.actualizarComentarios(),
        //     1000
        // );
        this.actualizarComentarios();
    }

    actualizarComentarios() {
        axios.get("http://localhost:8090/comentarios/estudiantes/2")
            .then(response => this.setState({ listaComentarios: response.data }))
            .catch(e => console.log(e));
    }

    handleSubmitResume = event => {
        this.props.history.push("/Teacher/Topic");
    };

    handleSubmitResults = event => {
        this.props.history.push("/Teacher/ResultResume");
    };

    handleSubmitComments = event => {
        this.props.history.push("/Teacher/Comments");
    };

    render() {
        return (
            <div>
                <Header3 resumen={"nav-item"} respuestas={"nav-item"} comentarios={"nav-item active"} handleSubmitResume={this.handleSubmitResume} handleSubmitResults={this.handleSubmitResults} handleSubmitComments={this.handleSubmitComments} />
                <section className="Main-Section">
                    <div id="Video">
                        <Player
                            ref="player"
                            autoPlay
                            fluid={false}
                            width={640}
                            height={360}
                        >
                            <source src={this.state.source} />
                            <ControlBar autoHide={false} />
                        </Player>
                    </div>
                    <div
                        className="card"
                        id="Comments-main-divs"
                    >
                        <div id="Comments-title">
                            <h4>Comentarios({this.state.listaComentarios.length})</h4>
                        </div>
                        <div id="Comments-div">
                            <div class="main">
                                <Comments listaComentarios={this.state.listaComentarios} />
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
