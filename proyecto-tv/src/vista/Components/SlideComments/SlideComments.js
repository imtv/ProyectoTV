import React, { Component } from "react";
import "./SlideComments.css";
import CommentStudent from "../CommentStudent/CommentStudent";
import axios from "axios";

class SlideComments extends Component {
  constructor(props) {
    super(props);
    this.mySlidenav = React.createRef();
    this.commentButton = React.createRef();
    this.openNav = this.openNav.bind(this);
    this.closeNav = this.closeNav.bind(this);
    this.state = {
      estado: "a",
      comentEstudiantes: []
    };
  }

  openNav() {
    if (this.state.estado === "a") {
      this.mySlidenav.current.style.width = "400px";
      this.commentButton.current.style.padding = "0px 400px 0px 0px";
      this.commentButton.current.src = "/assets/img/comentario_button2.png";
      this.setState({
        estado: "b"
      });
    }

    if (this.state.estado === "b") {
      this.mySlidenav.current.style.width = "0";
      this.commentButton.current.style.padding = "0px";
      this.commentButton.current.src = "/assets/img/comentario_button.png";
    }

    this.mySlidenav.current.style.width = "400px";
    this.commentButton.current.style.padding = "0px 400px 0px 0px";
    this.commentButton.current.src = "/assets/img/comentario_button2.png";
    this.setState({
      estado: "b"
    });
    console.log(this.state);
  }

  closeNav() {
    this.mySlidenav.current.style.width = "0";
    this.commentButton.current.style.padding = "0px";
    this.commentButton.current.src = "/assets/img/comentario_button.png";
    this.setState({
      estado: "a"
    });
    console.log(this.state);
  }

  componentWillMount() {
    this.timerID = setInterval(
      () => this.consultarAPI(),
      1000
    );
  }

  consultarAPI() {
    axios
      .get("http://192.168.1.22:8090/comentarios/estudiantes/" + this.props.fkVideo)
      .then(response =>
        this.setState({
          comentEstudiantes: response.data
        })
      )
      .catch(e => console.log(e));
  }

  render() {
    return (
      <div>
        <div ref={this.mySlidenav} id="mySlidenav" className="sidenav">
          <h2 className="ml-3 mb-4">Comentarios</h2>
          <a
            href="javascript:void(0)"
            className="closebtn"
            onClick={this.closeNav}
          >
            &times;
          </a>

          {this.state.comentEstudiantes.map(date => {
            const tiempo = new Date(parseInt(date.tiempo) * 1000);
            const mes = tiempo.getMonth() + 1;
            const dia = tiempo.getDate();
            const anio = tiempo.getFullYear();
            const fecha = mes + "/" + dia + "/" + anio;
            return (
              <CommentStudent
                key={date.idComentario}
                imagen={date.foto}
                nombre={date.nombreEstudiante + " " + date.apellidoEstudiante}
                comentario={date.comentario}
                tiempo={fecha}
              />
            );
          })}
        </div>
        <img
          ref={this.commentButton}
          src="/assets/img/comentario_button.png"
          className="imgComment"
          onClick={this.openNav}
          alt={""}
        />
      </div>
    );
  }
}

export default SlideComments;
