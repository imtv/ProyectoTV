import React, { Component } from "react";
import "./Incorrecto.css";

export default class Incorrecto extends Component {

    constructor() {
        super();
        this.state = {
        };
    }

    render() {
        return (
            <div className="fixed-top img" align="center">
                <img src="/assets/img/incorrecto.png"></img>
            </div>
        );
    }
}