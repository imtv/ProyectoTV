import React, { Component } from "react";
import "./InteractiveGesture.css";
import Hammer from "react-hammerjs";

export default class InteractiveGesture extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {};
    this.handlePan = this.handlePan.bind(this);
  }

  handlePan(event) {
    console.log(event);
    
  }

  render() {
    return (
      <div className="container-fluid p-0">
        <Hammer
          onPan={this.handlePan}
          options={{
            recognizers: {
              pan: { enable: true }
            }
          }}
        >
          <canvas className="canvas" />
        </Hammer>
      </div>
    );
  }
}
